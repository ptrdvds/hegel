# Copyright (C) 2020 Peter Davidse <hegel@esdivad.com>
#
# This file is part of hegel.

from setuptools import setup


def readme():
    with open('README.rst') as readme:
        return readme.read()


setup(name='hegel',
      version='0.1',
      description='Remote control for Hegel audio components.',
      long_description='README.rst',
      url='https://gitlab.com/ptrdvds/hegel',
      keywords='hegel remote h190 h390',
      author='Peter Davidse',
      author_email='hegel@esdivad.com',
      license='',
      packages=['hegel'],
      install_requires=[
            'markdown',
      ],
      include_package_data=True,
      zip_safe=False,
      classifiers=[
            'Development Status :: 4 - Beta ',
            'Intended Audience :: Developers',
            'Programming Language :: Python :: 3.7',
            'License :: OSI Approved :: MIT License',
            'Environment :: Console', ],
      ),
