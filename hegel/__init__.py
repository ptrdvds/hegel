# Copyright (C) 2020, Peter Davidse
# All rights reserved.
# This file is part of hegel.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

"""This module is used to remote control supported Hegel amplifiers.

It is recommended that you use the :func `hegel.create` to build the device
object. This results in cleaner code.

Examples:
    >>> import hegel
    >>> device = hegel.create('H390', '192.168.2.4')
    >>> device.failsafe
    >>> device.failsafe = True
    >>> device.failsafe_volume
    >>> device.failsafe_volume = 30
    >>> device.remote('volume', 10)
    >>> device.remote('i', 5)
    >>> device.remote('v')
    >>> device.remote('input')
    >>> device.capabilities()
"""
import re
import socket
from hegel.devices import devices

_SOCKET_CONNECT_TIMEOUT = 0.5
_DEFAULT_DEVICE_PORT = 50001
_FAILSAFE_VOLUME = 30


def create(model,
           ip_address,):
    """when creating a device only the type is required, all other properties
    of the device will be loaded from a configuration file.

    The *model* parameter specifies the type of audio device you are
    controlling. It is the model definition used by Hegel as in "Hegel H390"
    where H390 is representing the type. The model definition is not
    case sensitive, it will be converted with `lower()` to match the model
    attribute in the device file.
    """

    device_class = Hegel(model,
                         ip_address,)

    if knock(ip_address,
             device_class.port, ):
        return device_class

    return None


def knock(ip_address,
          port=_DEFAULT_DEVICE_PORT, ):
    try:
        sock = socket.socket(socket.AF_INET,
                             socket.SOCK_STREAM,)
        sock.settimeout(_SOCKET_CONNECT_TIMEOUT)
        result = sock.connect_ex((ip_address, port))
    except socket.gaierror:
        print("Hostname could not be resolved.")
        return False
    except socket.error:
        print(f'Could not connect to ip address.')
        return False
    finally:
        sock.close()

    if result == 0:
        return True
    else:
        print(f'Ip address has no open port "{port}"')

    return False


class Hegel:
    """This class represents a Hegel device. The device configuration is loaded
    from device.json with all definitions needed to remote control the device
    correctly without the need to know the inner workings or consulting the API.
    """
    def __init__(self,
                 model,
                 ip_address,):
        self.model = model.lower()
        self.ip_address = ip_address
        self.config = self._load()
        self.control_codes = self.config["control_codes"]
        self.port = self.config["port"]
        self._failsafe = True
        self._failsafe_volume = _FAILSAFE_VOLUME

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return f'Class "{self.__class__.__name__}", ' \
               f'model: "{self.model}", ip_address: "{self.ip_address}"'

    def _load(self):
        """This method loads the definition for the chosen model."""
        return load(self.model)

    def _query(self):
        """This method gets the current settings from the audio device."""
        for command in self.control_codes:
            received = send_command((self.ip_address,
                                     self.port,),
                                    command,
                                    "?",)
            self.control_codes[command]["current"] = received

    def capabilities(self,
                     formatted="json",):
        """This method returns the device capabilities."""

        self._query()

        # TODO format a nice table like output for print
        # print the device capabilities
        if formatted == "print":
            print(self.control_codes)

        if formatted == "" or formatted == "json":
            return self.control_codes

        return None

    def remote(self,
               command,
               value="?",):
        """This method sets or gets a single capability on the audio device."""

        if command[:1] not in self.control_codes:
            print(f"Command {command} not supported by device {self.model}.")
            return None
        else:
            command = command[:1]

        if value != "?":
            try:
                value = int(value)
            except ValueError as error:
                print("Value must be an integer or a numeric string.")
                return None

            # check if value is in range
            minimum = int(self.control_codes[command[:1]]["min"])
            maximum = int(self.control_codes[command[:1]]["max"])
            if minimum <= value <= maximum:
                pass
            else:
                print(f"Value {value} is not in allowed range {minimum} - {maximum}.")
                return None

            # check if failsafe_volume
            if command == "v" and value > self._failsafe_volume and self._failsafe:
                print(f"Refusing to set volume to {value}, failsafe volume is {self.failsafe_volume}.")
                value = self._failsafe_volume

        return send_command((self.ip_address,
                             self.port,),
                            command,
                            value,)

    @property
    def failsafe(self):
        """This method gets failsafe value for the audio device."""
        return self._failsafe

    @failsafe.setter
    def failsafe(self,
                 value):
        """This method sets failsafe value for the audio device."""
        self._failsafe = value

    @property
    def failsafe_volume(self):
        """This method gets failsafe volume value for the audio device."""
        return self._failsafe_volume

    @failsafe_volume.setter
    def failsafe_volume(self,
                        value):
        """This method sets failsafe volume value for the audio device."""
        self._failsafe_volume = value


def send_command(device_address,
                 command,
                 value,):
    """This function sends and receives control messages."""

    # build the message to send to the audio device
    message = f'-{command}.{value}\r'.encode()

    try:
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET,
                             socket.SOCK_STREAM,)
        sock.settimeout(_SOCKET_CONNECT_TIMEOUT)
        sock.connect(device_address)
    except ConnectionRefusedError as error:
        raise error
    except OSError as error:
        print("There is an error. Maybe the host is not reachable!")
    else:
        try:
            sock.sendall(message)

            # Look for the response
            amount_received = 0
            amount_expected = len(message)

            while amount_received < amount_expected:
                data = sock.recv(16)
                amount_received += len(data)
        finally:
            sock.close()

            # get the payload from the received packet
            return re.findall(r"\.(\d+)",
                              str(data),
                              )[0]


def load(model):
    """This function returns settings for the required audio device."""
    try:
        # Check if the configured device is in the definition file
        devices[model]
    except KeyError as error:
        print(f"Model {model} not found")
        return None
    else:
        return devices[model]
