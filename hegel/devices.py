# Copyright (C) 2020, Peter Davidse
# All rights reserved.
# This file is part of hegel.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

"""This has the properties for all supported Hegel models."""

devices = {
  "metadata": {
    "version": "0.01"
  },
  "h190": {
    "port": 50001,
    "control_codes": {
      "p": {
        "command": "power",
        "datatype": "bool",
        "min": 0,
        "max": 1,
        "description": "Power",
        "range": "1 / 0",
        "type": "t",
        "function": "ON / OFF",
        "comments": "The Hegel H190 does not have a true standby mode. However, the power command still functions by disconnecting all outputs and dimming the display",
        "example": ""
      },
      "i": {
        "command": "input",
        "datatype": "int",
        "min": 1,
        "max": 9,
        "description": "Source Input",
        "range": "1 - 9",
        "type": "u / d",
        "function": "Set to input number [parameter]",
        "1": "Balanced",
        "2": "Analog 1",
        "3": "Analog 2",
        "4": "Coaxial",
        "5": "Optical 1",
        "6": "Optical 2",
        "7": "Optical 3",
        "8": "USB",
        "9": "Network",
        "comments": "Balanced is input number 1, Analog 1 input number 2, and so on.",
        "example": "Sending -i.4<CR> sets the input selector to coaxial input"
      },
      "v": {
        "command": "volume",
        "datatype": "int",
        "min": 0,
        "max": 100,
        "description": "Volume Control",
        "range": "0 - 100",
        "type": "u / d",
        "function": "Set volume to [parameter]%",
        "comments": "The conversion from volume% to volume level rounds up. Conversion from volume level to volume % rounds down.",
        "example": "If the device max volume is set to 70, sending -v.96<CR> will set it to volume level 68 and return -v.97<CR>."
      },
      "m": {
        "command": "mute",
        "datatype": "bool",
        "min": 0,
        "max": 1,
        "description": "Volume Mute",
        "range": "1 / 0",
        "type": "t",
        "function": "ON / OFF",
        "comments": "",
        "example": ""
      }
    }
  },
  "h390": {
    "port": 50001,
    "control_codes": {
      "p" : {
        "label": "power",
        "datatype": "bool",
        "min": 0,
        "max": 1,
        "description": "Power",
        "range": "1 / 0",
        "type": "t",
        "function": "ON / OFF",
        "comments": "The Hegel H390 does not have a true standby mode. However, the power command still functions by disconnecting all outputs and dimming the display",
        "example": ""
      },
      "i": {
        "label": "input",
        "datatype": "int",
        "min": 1,
        "max": 10,
        "description": "Source Input",
        "range": "1 - 10",
        "type": "u / d",
        "function": "Set to input number [parameter]",
        "1": "XLR",
        "2": "Analog 1",
        "3": "Analog 2",
        "4": "BNC",
        "5": "Coaxial",
        "6": "Optical 1",
        "7": "Optical 2",
        "8": "Optical 3",
        "9": "USB",
        "10": "Network",
        "comments": "XLR 1 is input number 1, XLR 2 input number 2, and so on.",
        "example": "Sending -i.4<CR> sets the input selector to BNC input"
      },
      "v": {
        "label": "volume",
        "datatype": "int",
        "min": 0,
        "max": 100,
        "description": "Volume Control",
        "range": "0 - 100",
        "type": "u / d",
        "function": "Set volume to [parameter]%",
        "comments": "The conversion from volume% to volume level rounds up. Conversion from volume level to volume % rounds down.",
        "example": "If the device max volume is set to 70, sending -v.96<CR> will set it to volume level 68 and return -v.97<CR>."
      },
      "m": {
        "label": "mute",
        "datatype": "bool",
        "min": 0,
        "max": 1,
        "description": "Volume Mute",
        "range": "1 / 0",
        "type": "t" ,
        "function": "ON / OFF",
        "comments": "",
        "example": ""
      }
    }
  }
}
