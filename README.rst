# Hegel network controller for Python

This repository contains the files to control a Hegel Audio device.

## Supported devices

+ Hegel H190 Integrated Amplifier
+ Hegel H390 Integrated Amplifier
+ others need a definition in the definitions file

## Supported python versions

+ Python 3.7 and higher

## Installation

    pip install hegel

## Quick Example  

    >>> import hegel
    >>> device = hegel.create('H390', '192.168.2.4')
    >>> device.failsafe
    >>> device.failsafe = True
    >>> device.failsafe_volume
    >>> device.failsafe_volume = 30
    >>> device.remote('volume', 10)
    >>> device.remote('i', 5)
    >>> device.remote('v')
    >>> device.remote('input')
    >>> device.capabilities()

### config.ini settings


### links
ip control codes:   

[H190](https://support.hegel.com/component/jdownloads/send/3-files/12-h190-ip-control-codes)
[H390](https://support.hegel.com/component/jdownloads/send/3-files/82-h390-ip-control-codes)
